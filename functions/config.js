var config = {};

config.schedule = new Array({name:"Sunday", open:"2:00pm", close:"5:00pm", closed:false},
                            {name:"Monday", open:"10:00am", close:"8:00pm", closed:false},
                            {name:"Tuesday", open:"10:00am", close:"8:00pm", closed:false},
                            {name:"Wednesday", open:"10:00am", close:"5:00pm", closed:false},
                            {name:"Thursday", open:"10:00am", close:"8:00pm", closed:false},
                            {name:"Friday", open:"10:00am", close:"5:00pm", closed: false},
                            {name:"Saturday", open:"10:00am", close:"5:00pm", closed: false}); 
                            
config.hours_fallback = `We are open 10:00am to 8:00pm Monday, Tuesday, and Thursday; 10:00am 
                        to 5:00pm Wednesday, Friday, and Saturday; and from 2:00pm to 5:00pm on Sunday. Thank you!`;

config.children_data = `Thank you for your interest in our children's programs! Our full schedule of programs is available at 
                        https://gchrl.org/department/childrens - if you have more than one child, you are welcome to bring 
                        them all to any program.`;
                            
config.branch_name = "Columbia County Library";

config.branch_code = "GCHR-CCO";

config.web_site = "gchrl.org";

config.evergreen_server = "gapines.org";

module.exports = config;