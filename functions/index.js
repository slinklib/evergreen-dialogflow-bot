const functions = require("firebase-functions");
const {WebhookClient} = require("dialogflow-fulfillment");
// const {Card, Suggestion} = require('dialogflow-fulfillment');
const catalogSearch = require("rss-to-json");
const config = require("./config");



"use strict";

process.env.DEBUG = "dialogflow:debug";

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//


exports.libraryChat = functions.https.onRequest((req, res) => {
    
    const agent = new WebhookClient({ request: req, response: res });
    
    const schedule = config.schedule; 
    let name = "";
    let this_day = new Date();
    
    function welcome(agent) {
        if (agent.parameters["given-name"]) {
            name = agent.parameters["given-name"];
            agent.add(`Welcome to ${config.branch_name}, ${name}!`);
            agent.add(`Hello ${name}! Thank you for contacting the ${config.branch_name}!`);
            agent.add(`Thank you for using ${config.branch_name}, ${name}!`);
        }
        agent.add(`Welcome to ${config.branch_name}!`);
        agent.add(`Thank you for contacting ${config.branch_name}!`);
    }
    
    function fallback(agent) {
        agent.add("I didn't quite get that, could you repeat your question?");
        agent.add("I'm sorry, could you repeat that question?");
    }
    
    function hours(agent) {
        if (agent.parameters["date"]) {
            this_day = new Date(agent.parameters["date"]);
            var weekday = schedule[this_day.getDay()];
            var response = weekday.closed ? `We are closed on ${weekday.name}` :  `Our hours for ${weekday.name} are ${weekday.open} to ${weekday.close}. For closed dates and holiday schedules, please visit our website at ${config.web_site}. Thank you!`;
            agent.add(response);
        }
        else {
            agent.add(config.hours_fallback);
        }
    }
    
    function child_activities(agent) {
        agent.add(config.children_data);
    }
    
    function catalog_search(agent) {
        var itemSubject = agent.parameters["item-subject"] ? `+${agent.parameters["item-subject"]}` : "";
        var itemTitle = agent.parameters["item-title"] ? `+${agent.parameters["item-title"]}` : "";
        var itemType = agent.parameters["item-type"] ? `+${agent.parameters["item-type"]}` : "";
        var authorName = "";
        if (agent.parameters["author-name"]) {
           authorName = agent.parameters["author-name"]["name"] ? `+${agent.parameters["author-name"]["name"]}+${agent.parameters["author-name"]["last-name"]}` : `+${agent.parameters["author-name"]["last-name"]}`;
        }
        var searchString = "";
        if (itemSubject.length > 0) { searchString += itemSubject; }
        if (itemTitle.length > 0 ) { searchString += itemTitle; }
        if (authorName.length > 0) { searchString += authorName; }
        if (itemType.length > 0) { searchString += itemType; }
        var url = `https://${config.evergreen_server}/opac/extras/opensearch/1.1/-/rss2-full?searchTerms=site(${config.branch_code})${encodeURIComponent(searchString)}&searchClass=keyword`;
        var itemList = [];
        var chatResponse = "this was untouched";
        
        return new Promise((resolve, reject) => {
            catalogSearch.load(url, (err, rss) => {
                if (err) {
                    reject(Error("url failed!"));
                }
                else {
                    itemList = rss;
                    if (itemList.items.length > 0) {
                        if (itemList.items.length === 1) {
                            chatResponse = "The only item found for that search is ";
                        }
                        else if (itemList.items.length < 10) {
                            chatResponse = "The first few items in your search are ";
                        }
                        else {
                            chatResponse = "The first ten items in your search are ";
                        }
                        itemList.items.forEach( (item, index) => {
                           chatResponse = chatResponse + item.title.replace(/\//g, "").trim();
                           if (index + 1 < itemList.items.length) {
                               chatResponse += ", ";
                           } else {
                               chatResponse += ".";
                           }
                        });
                    }
                    else {
                        chatResponse = "We're sorry, there are no items matching that search at our library!";
                    }
                    console.log(rss);
                    resolve(chatResponse);
                }
            });
        });
    }
    
    function catalog_response() {
        catalog_search(agent).then((response) => {
            res.json({ "fulfillmentText": response });
            res.status(200).send();
            return 1;
        }), (error) => {
            console.log("failed", error);
        };
    }
    
    function chooseHandler(agent) {
        var intentRoute = "";
        switch (agent.intent) {
            case "Default Welcome Intent": 
                intentRoute = "welcome";
                break;
            case "Default Fallback Intent": 
                intentRoute = "fallback";
                break;
            case "HoursInquiry":
                intentRoute = "hours";
                break;
            case "Child Activities":
                intentRoute = "children";
                break;
            case "Catalog Search":
                intentRoute = "catalog";
                break;
        }
        return new Promise ((resolve, reject) => {
            if (intentRoute.length > 0) {
                resolve(intentRoute);
            }
            else {
                reject(Error("Route Not Found"));
            }
        });
    }
    
    function assignResponse(intentRoute, agent) {
        switch (intentRoute) {
            case "welcome":
                agent.handleRequest(welcome);
                break;
            case "fallback":
                agent.handleRequest(fallback);
                break;
            case "hours":
                agent.handleRequest(hours);
                break;
            case "children":
                agent.handleRequest(child_activities);
                break;
            case "catalog":
                catalog_response();
                break;
            default:
                Error("Intent Not Found");
        }
        
    } 
    
    chooseHandler(agent)
    .then((response) => {
        assignResponse(response, agent);
        console.log("Response Processed");
        return true;
    }), (error) => {
        console.log("Failed!", error);
    };
        
  
});