### DialogFlow Agent for Evergreen ILS Libraries

#### Table of Contents

##### A) Description

##### B) Requirements

##### C) Installation

##### D) Example Intents

##### E) Known Issues



###### A) Description

This provides a basic chatbot for DialogFlow with interaction with an Evergreen ILS server. It also provides responses to questions about hours and children's activities. In addition, the bot will respond with results from questions concerning the presence of materials at
library pulled from the Evergreen server.

###### B) Requirements

To run this agent, you will need a Firebase account, a DialogFlow account, Node.js > 8.0.x, and NPM >= 3.10.x. This agent was initially created using the c9.io online IDE, which is a simple way to create a Node.js environment.

###### C) Installation

    1) Clone the repository to your local environment using:
    
       git clone git@gitlab.com:slinklib/evergreen-dialogflow-bot.git
       
    2) Create a new DialogFlow agent via https://dialogflow.com.
    
    3) Under the agent settings, select "Export and Import", "Import From Zip", and then upload the 
    library-chatbot.zip file.
    
    4) Change directories to /functions and run:
    
        npm install
        
    5) Run 'firebase init' to create a new Firebase Web Functions service.
    
    7) Edit 'functions/config.js' to reflect your library's settings and save.
    
    7) Run 'firebase deploy' and note the target URL given at the end of the setup.
        
    8) Under "Fulfillment" in your Dialogflow Agent Panel, enable the Webhook and set the URL to the target 
    produced by 'firebase init'.
    
    9) Test!
    
###### D) Example Intents

1) Do you have any books about knitting?

2) What are your hours on Sunday?

3) Do you have any books by James Patterson?

3) Do you offer any storytimes?

5) Do you have The Metamorphosis by Kafka?
    
###### E) Known Issues

    1) The search function does not do well with searches by subject presently.
    
    2) The search function does not specify the difference between keyword, author, and title searches.